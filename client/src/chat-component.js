// @flow

import * as React from 'react';
import { Component } from 'react-simplified';
import chatService from './chat-service';
import { Alert } from './widgets';
const { v4: uuidv4 } = require('uuid');

export class Chat extends Component {

  subscription = null;
  connected = false;
  chatMessages = [];

  statuses = {
    NONE: 'NONE',
    PENDING: 'PENDING',
    APPROVED: 'APPROVED'
  }

  statusColors = {
    [this.statuses.NONE]: 'gray',
    [this.statuses.PENDING]: 'yellow',
    [this.statuses.APPROVED]: 'green',

  }

  user = '';
  loggedInStatus = this.statuses.NONE;

  users = new Set();

  logIn(event) {
    if (this.loggedInStatus !== this.statuses.NONE) return;
    event.preventDefault();
    let userHash = `#${uuidv4().split('-')[0]}`;
    const userName = this.user + userHash;
    chatService.send({ user: userName, logInRequest: true })
    this.loggedInStatus = this.statuses.PENDING;
    this.user = userName;
  }

  sendMessage(event) {
    console.log(this.users)
    if (this.loggedInStatus !== this.statuses.APPROVED) return;

    event.preventDefault();
    const messageValue = event.currentTarget.querySelector('textarea').value;

    if (this.users.has(this.user)) {
      chatService.send({ user: this.user, message: messageValue, id: uuidv4() })
    }
  }

  render() {
    return (
      <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
        <h2>Chaturmate</h2>
        <section>
          <h5>Log in:</h5>
          <form onSubmit={e => this.logIn(e)} style={{ border: `3px solid ${this.statusColors[this.loggedInStatus]}`, display: "inline-block" }}>
            <input type="text" value={this.user} onChange={e => this.user = e.currentTarget.value} placeholder="Enter your username" disabled={this.loggedInStatus !== this.statuses.NONE} />
            <button disabled={this.loggedInStatus !== this.statuses.NONE}>Go!</button>
          </form>
        </section>
        {
          this.loggedInStatus === this.statuses.APPROVED ?
            <div>
              <section style={{ marginTop: '3rem', maxHeight: '15rem', overflowY: 'auto', width: '80vw' }}>
                {
                  this.chatMessages.length === 0 ?
                    <h4>Ingen chatmeldinger enda :/</h4> :
                    <>
                      {
                        this.chatMessages.map(message => {
                          return (
                            <div key={message.id}><b>{message.user}</b>: {message.message}</div>
                          )
                        })
                      }
                    </>
                }
              </section>
              <form onSubmit={e => this.sendMessage(e)} style={{ display: 'grid', marginTop: '4rem' }}>
                <textarea rows={5}></textarea>
                <button>Send message</button>
              </form>

              <section style={{ marginTop: '2rem', maxHeight: '5rem', overflowY: 'auto' }}>
                <h4>Logged in users:</h4>
                {
                  /**
                   * Note, this is not UX-friendly. If you enter the site after someone, then you won't know that
                   * they're logged in until they've sent a message. I just wanted to try to implement something
                   * that shows current users :)
                   */
                  [...this.users].map(user => <span key={user} style={{ marginRight: '1rem' }}>{user}</span>)
                }
              </section>
            </div>
            : null
        }

      </div>
    );
  }

  mounted() {
    // Subscribe to chatService to receive events from Whiteboard server in this component
    this.subscription = chatService.subscribe();

    // Called when the subscription is ready
    this.subscription.onopen = () => {
      this.connected = true;
    };

    // Called on incoming message
    this.subscription.onmessage = (message) => {
      if (message.logInRequest) {
        this.users.add(message.user)
        if (message.user === this.user && this.loggedInStatus !== this.statuses.APPROVED) {
          this.loggedInStatus = this.statuses.APPROVED;
        }
      }

      if (this.loggedInStatus === this.statuses.APPROVED && message.hasOwnProperty('message')) {
        this.chatMessages = [...this.chatMessages, message]

        if (!this.users.has(message.user)) {
          this.users.add(message.user)
        }
      }
    };

    // Called if connection is closed
    this.subscription.onclose = (code, reason) => {
      this.connected = false;
      Alert.danger('Connection closed with code ' + code + ' and reason: ' + reason);
    };

    // Called on connection error
    this.subscription.onerror = (error) => {
      this.connected = false;
      Alert.danger('Connection error: ' + error.message);
    };
  }

  // Unsubscribe from chatService when component is no longer in use
  beforeUnmount() {
    chatService.unsubscribe(this.subscription);
  }
}
